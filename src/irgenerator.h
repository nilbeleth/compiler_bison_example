#ifndef IRGENERATOR_H
#define IRGENERATOR_H
#include <stack>

#include "file.h"
#include "stack.h"
#include "scanner.h"
#include "controlstructure.h"


namespace frontend
{



/**
 * @brief 3AC generator.
 *
 * After successful recognition of a grammar rule IRGenerator
 * is responsible for generating an adequate 3AC (intermediate
 * presentation used within this compiler).
 */
class IRGenerator
{
    public:
        /** Default constructor */
        IRGenerator() {}

        /** Constructor with pre-set file table. */
        IRGenerator(symbols::FileTable* table);

        /** Default destructor */
        virtual ~IRGenerator();



        /**
         * While parsing we find a new file we need to open it.
         * @param name      Filename
         */
        void addFile(const std::string name);


        /**
         * Whole file parsed. Close it.
         */
        void closeFile() { m_file = NULL; }



        /**
         * Select the correct function. In case there aren't any
         * create one.
         * @param tok   Function identifier.
         */
        void selectFunct(const Token tok);

        /** Set function as declared but not defined. */
        void setFuncDeclared();

        /** Load neccessary stuff to parse a function body. */
        void enterFunct();
        /** Unload all stuff regarding parsed function. */
        void leaveFunct();

        /**
         * Add a new argument for function.
         * @param tok   Token with argument's id.
         */
        void addArg(const Token tok);

        /** Just declared a argument for function with temporary name. */
        void declArg();



        /**
         * Declare a new variable for topmost block.
         * @param tok   Variable identification.
         */
        void declareVar(const Token tok);
        void declareVar(const Token tok1, const Token tok2);

        /**
         * Define a new constant.
         * @param tok   Token bearing constant value and type.
         */
        void defConst(const Token tok);

        /**
         * Find a symbol and push it on variable stack.
         * @param tok   Symbol identification.
         */
        void loadSymbol(const Token tok);


        /**
         * Generate a new assigment instruction.
         * @param tok   The destination of assigment.
         */
        void assign(const Token tok);

        /**
         * Generate a return instruction.
         * @param expr  Was return called with an expression after?
         */
        void ret(const bool expr = false);

        /**
         * Generate a jump to begin or end of the block (also check if we are in block).
         */
        void jump(const bool daco);

        void enterIfCond();
        void enterIfTrue();
        void enterIfFalse();


        /**
         * Evaluate a unary operation.
         * @param tok   The type of operation (e.g. -, !,...).
         */
        void evalUnaOp(const Token tok);

        /**
         * Evaluate a binary operation.
         * @param tok   The type of operation (e.g. +, -, *,...).
         */
        void evalBinOp(const Token tok);



        /**
         * Hard fix to propagate ID while parsing declaration.
         */
        void saveID(const Token tok);

        /** Unset type. */
        void unsetType();
        /** Set a new data type (even void). */
        void setType(const symbols::eDataType type);


    private:
        symbols::eDataType m_type;                  /**< temporary type storage */
        Token m_token;                              /**< store id through some rules */
        unsigned m_id;                              /**< auxilary variable for generating unique names */

        symbols::FileTable* m_table;                /**< Table of all source files */
        symbols::File* m_file;                      /**< Reference to actually opened file */
        symbols::Function* m_function;              /**< Reference to actually parsed function */
        Stack<symbols::Block*> m_blocks;            /**< Loaded symbol blocks */
//        Stack<symbols::ControlStructure> m_controls;/**< Loaded control flow statements */
        std::stack<symbols::Symbol*> m_symStack;    /**< Stack for used symbols */


        /**
         * Generate a new unique name for internal variable.
         * @param orig      The origin of creation for variable.
         * @param lineno    The line on which it has occured.
         */
        std::string generateVariable(const symbols::eOrigin orig, const unsigned int lineno);


        /**
         * Convert the type of one of these symbols. Also replace that one on stack.
         */
        void convert(const symbols::Symbol* op1, const symbols::Symbol* op2);
};

}   // end namespace frontend


#endif // IRGENERATOR_H
