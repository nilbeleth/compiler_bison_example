#ifndef FUNCTION_H
#define FUNCTION_H
#include <map>
#include <vector>

#include "controlstructure.h"
#include "symbol.h"
#include "stack.h"



namespace symbols
{


/**
 *
 */
typedef std::vector<Instruction*> tInstrList;


/**
 * @brief The symbol for function.
 *
 * Class Function represents a function originally parsed
 * from one of the source file. Within this class we store
 * all necessary data about a single function like it's
 * argument, return type or instruction list.
 */
class Function
{
    public:
        /** Default constructor */
        Function(const std::string name, const eDataType type = tVoid);

        /** Default destructor */
        ~Function();


        /**
         * Get function name.
         */
        std::string getName() const { return m_name; }

        /** Is function declared or defined? */
        eUsage getDeclared() const { return m_usage; }

        /** How much arguments does this function have? */
        unsigned int getArgCount() const { return m_args.size(); }

        /** What's the function return data type? */
        eDataType getRetType() const { return m_retType; }

        /**
         * Get the symbol table for this function.
         * @return  Root symbol table of function.
         */
        Block* getFunctRootBlock() { return m_functRootBlock; }

        /**
         *
         */
        void getInstrList();


        //
        // Frontend use ony
        //
        /**
         * Mark this function as declared.
         */
        void setDeclared() { m_usage = uDeclared; }

        /**
         * Mark this function as declared and defined.
         */
        void setDefined() { m_usage = uDefined; }


        /**
         * Add a new argument for this function.
         * @param type      Argument data type
         * @param name      Argument name
         */
        void addArg(const eDataType type, const std::string name);

        /**
         * There is already declared all argument. This function only
         * name them one by one.
         * @param type
         * @param name
         */
        void nameArg(const eDataType type, const std::string name);

        /**
         * Have been all arguments renamed?
         * @return      True if there is no unnamed arguments (success).
         */
        bool testArgRenamed() const;


        /**
         *
         */
        void instr_add(const eOpType instr, const eDataType type, const Operand* res, const Operand* op1 = NULL, const Operand* op2 = NULL);


        void enterIfInit();
        void enterIfTrue();
        void enterIfFalse();
        void leaveIf();


        /**
         *
         */
        std::string asString(const unsigned int indent = 3) const;


    private:
        std::string m_name;
        eUsage m_usage;                         /**< Was function declared or defined? */
        std::vector<Symbol*> m_args;            /**< A reference to function parameters. */
        tInstrList m_instrs;                    /**< The list of instructions defining function. */
        Block* m_functRootBlock;                /**< Function root namespace/block */
        eDataType m_retType;                    /**< The function return type */

        Stack<ControlStructure> m_controls;     /**< Loaded control flow statements */

        bool m_called;                          /**< Was function called? */
};

}   // end namespace symbols

#endif // FUNCTION_H
