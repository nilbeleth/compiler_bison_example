#include <sstream>

#include "instruction.h"
#include "logger.h"
#include "utils.h"


using namespace std;
using namespace symbols;



Operand::Operand()
{

}


Instruction::Instruction()
    : m_instr(iNop), m_type(tUnset), m_result(NULL), m_operand1(NULL), m_operand2(NULL)
{
    //ctor
}


Instruction::Instruction(const eOpType instr, const eDataType type, const Operand* res, const Operand* op1, const Operand* op2)
    : m_instr(instr), m_type(type), m_result(res), m_operand1(op1), m_operand2(op2)
{

}


Instruction::~Instruction()
{
    //dtor
}


string Instruction::asString() const
{
    stringstream ss;

    // first instruction type
    ss << m_instr << ": ";

    if( m_result != NULL )
        ss << m_result->asString();

    if( m_operand1 != NULL )
        ss << ", " << m_operand1->asString();

    if( m_operand2 != NULL )
        ss << ", " << m_operand2->asString();


    return ss.str();
}


ostream& operator<<(std::ostream& os, symbols::eOpType type)
{
    switch(type)
    {
        case iNop:
            os << "NOP";
            break;
        case iHalt:
            os << "HALT";
            break;
        case iCall:
            os << "CALL";
            break;
        case iReturn:
            os << "RET";
            break;
        case iVar:
            os << "VAR";
            break;
        case iArg:
            os << "ARG";
            break;
        case iFun:
            os << "FUN";
            break;
        case iAssign:
            os << "ASSIGN";
            break;
        case iAdd:
            os << "ADD";
            break;
        case iSub:
            os << "SUB";
            break;
        case iMult:
            os << "MULT";
            break;
        case iDiv:
            os << "DIV";
            break;
        case iMod:
            os << "MOD";
            break;
        case iGoto:
        case iBrIfNot:
            WARNING("Not yet implemented.")
        case iCast:
            os << "CAST";
            break;
        case iLabel:
            os << "LABEL";
            break;
        default:
            WARNING("Unknown instruction type.")
            break;
    }

    return os;
}
