#ifndef UTILS_H
#define UTILS_H
#include <string>



/**
 * Auxilary function for generating blank spaces.
 * @param level     Level of indentation
 * @return      Number of tabs according to level
 */
inline std::string getIndent(const unsigned int level)
{
    std::string res("");

    for( unsigned int i = 0; i < level; i++)
    	res += "    ";

    return res;
}


/**
 *
 */
class Utils
{
    public:
        Utils();
};

#endif // UTILS_H
