#include <sstream>

#include "controlstructure.h"


using namespace std;
using namespace symbols;



ControlStructure::ControlStructure()
{
    m_start = new Instruction(iLabel, tVoid, NULL, NULL, NULL);
    m_end = new Instruction(iLabel, tVoid, NULL, NULL, NULL);
}


ControlStructure::~ControlStructure()
{
    m_start = NULL;
    m_end = NULL;
}


IfStructure::IfStructure()
    : ControlStructure()
{

}


WhileStructure::WhileStructure()
    : ControlStructure()
{

}
