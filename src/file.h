#ifndef FILE_H
#define FILE_H
#include <map>
#include <string>

#include "function.h"



namespace symbols
{


/**
 *
 */
class File
{
    public:
        /** Default constructor */
        File(const std::string name);


        /** Default destructor */
        ~File();

        Function* addFunction(const std::string name, const eDataType type);
        Function* findFunction(const std::string name);

        Block* getRootBlock() { return m_rootBlock; }

        /** */
        std::string asString(const unsigned int indent = 1) const;


    protected:
    private:
        std::string m_filename;
        std::map<std::string, Function*> m_functTable;
        Block* m_rootBlock;
};


/**
 * @brief
 *
 *
 */
class FileTable
{
	public:
		FileTable();
		~FileTable();

		File* addFile(const std::string name);
		File* findFile(const std::string file);

		bool testMainPresence() const;

		std::string asString() const;

	private:
		std::map<std::string, File*> m_table;
};

}   // end namespace symbols

#endif // FILE_H
