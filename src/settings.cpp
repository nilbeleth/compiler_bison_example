#include <cstdlib>
#include <getopt.h>
#include <iostream>

#include "settings.h"
#include "debug.h"


using namespace std;



Settings::Settings()
    : m_bVerbose(0), m_bDebug(false), m_iOptimLevel(0)
{
    m_sInput = "";
    m_sOutput = "";
}


Settings::~Settings()
{
}


Settings& Settings::getInstance()
{
    static Settings instance;
    return instance;
}


int Settings::parseArgs(int argc, char* argv[])
{
    Settings &s = Settings::getInstance();
    opterr = 0;     // disable getopt error msg

    // implicit options
    s.m_bVerbose = false;
    s.m_bDebug = false;
    s.m_iOptimLevel = 0;
    s.m_sOutput = "out.asm";


    // definitions of allowed options
    const char *shortOption = "hvDO:";

    const struct option longOption[] =
    {
        { "help",    no_argument,       NULL, 'h' },
        { "verbose", no_argument,       NULL, 'v' },
        { "debug",   no_argument,       NULL, 'D' },
        { "optim",   required_argument, NULL, 'O' },
        { 0,         0,                 0,    0   },
    };


    // while there is something to parse
    int next = 0;
    while( (next = getopt_long(argc, argv, shortOption, longOption, NULL)) != -1 )
    {
        switch(next)
        {
            case 'v':
                m_bVerbose++;
                if( m_bVerbose > 3 )
                    WARNING("Verbose level too high.")
                break;
            case 'D':
                m_bDebug = true;
                break;
            case 'O':
                m_iOptimLevel = atoi(optarg);
                break;
        	case 'h':
                printHelp();
                exit(0);
            case ':':
                cerr << "Missing argument: -" << (char)optopt << endl;
                return 1;
        	case '?':
                cerr << "Unrecognized option: -" << (char)optopt << endl;
                return 1;
        	default:
                ERROR("Default option while parsing arguments.")
                printHelp();
                return 1;
        }
    }


    // read input file as well (and output maybe)
    if( optind < argc )
        m_sInput = argv[optind++];
    else
    {
        cerr << "Missing input file." << endl;
        return 1;
    }

    if( optind < argc )
        m_sOutput = argv[optind++];

    if( optind != argc )
        WARNING("Too many arguments.")


    return 0;
}


void Settings::printHelp() const
{
    cout << "Vype compilator translates a source file in C-like language to MIPS32 assambler." << endl;
    cout << endl;
    cout << "Usage:" << endl;
    cout << "vype [options] input_file [output_file]" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << "\t-h,--help        - Print this help." << endl;
    cout << "\t-v,--verbose     - Produce more verbose output." << endl;
    cout << "\t-D,--debug       - Print debugging information as well." << endl;
    cout << "\t-O?              - Set optimalizatation level (default 0)." << endl;
}
