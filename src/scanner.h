#ifndef SCANNER_H
#define SCANNER_H
#include <string>



namespace frontend
{


/**
 * Token enumeration
 */
enum eTokens
{
   T_NULL = 0,

    /* 1 */
   T_ID,                ///< identifier
   T_NUM,               ///< decimal literal
   T_CHAR,              ///< character literal
   T_STRING,            ///< string literal

    /* 5*/
   T_OP_LPAR,           ///< operator '('
   T_OP_RPAR,           ///< operator ')'
   T_OP_LBRA,           ///< operator '{'
   T_OP_RBRA,           ///< operator '}'
   T_OP_ASSIGN,         ///< operator '='
   T_OP_PLUS,           ///< operator '+'
   T_OP_MINUS,          ///< operator '-'
   T_OP_TIMES,          ///< operator '*'
   T_OP_DIVIDE,         ///< operator '/'
   T_OP_MODULO,         ///< operator '%'
   T_OP_GREATER,        ///< operator '>'
   T_OP_GREATER_EQ,     ///< operator '>='
   T_OP_LESS,           ///< operator '<'
   T_OP_LESS_EQ,        ///< operator '<='
   T_OP_EQUAL,          ///< operator '=='
   T_OP_NONEQUAL,       ///< operator '!='
   T_OP_NEG,            ///< operator '!'
   T_OP_OR,             ///< operator '||'
   T_OP_AND,            ///< operator '&&'
   T_OP_COLON,          ///< operator ','
   T_OP_SEMICOLON,      ///< operator ';'

    /* 26 */
   T_KW_BREAK,          ///< keyword 'break'
   T_KW_CHAR,           ///< keyword 'char'
   T_KW_CONTINUE,       ///< keyword 'continue'
   T_KW_ELSE,           ///< keyword 'else'
   T_KW_FOR,            ///< keyword 'for'
   T_KW_IF,             ///< keyword 'if'
   T_KW_INT,            ///< keyword 'int'
   T_KW_RETURN,         ///< keyword 'return'
   T_KW_SHORT,          ///< keyword 'short'
   T_KW_STRING,         ///< keyword 'string'
   T_KW_UNSIGNED,       ///< keyword 'unsigned'
   T_KW_VOID,           ///< keyword 'void'
   T_KW_WHILE,          ///< keyword 'while'

    /* 39 */
   T_FN_PRINT,          ///< built-in function 'print()'
   T_FN_READ_CHAR,      ///< built-in function 'read_char()'
   T_FN_READ_INT,       ///< built-in function 'read_int()'
   T_FN_READ_STRING,    ///< built-in function 'read_string()'
   T_FN_GET_AT,         ///< built-in function 'get_at()'
   T_FN_SET_AT,         ///< built-in function 'set_at()'
   T_FN_STRCAT          ///< built-in function 'strcat()'
};



/**
 * @brief Token returned by scanner.
 *
 * Token class represents a token produce by lexical analysis.
 * It also holds token type and it's attribute or value. This
 * token is afterwards presented to parser to futher analysis.
 */
class Token
{
	public:
        eTokens type;           /**< The token type (like id, number, operator,...) */
        int lineno;             /**< Where exactly is the token in source file? */

        char cValue;            /**< Token value if it's a character. */
        int iValue;             /**< Token value if it's a number. */
        std::string sValue;     /**< Token value for string or identifier's name. */


        /** Default constructor */
        Token()
            : type(T_NULL), lineno(0), cValue('\0'), iValue(0), sValue("")
        { }

        /** Constructor with parameters */
        Token( eTokens type_, int lineno_, char c, int i, std::string s )
            : type(type_), lineno(lineno_), cValue(c), iValue(i), sValue(s)
        { }

        /** Assignment constructor */
        Token(const Token &s)
            : type(s.type), lineno(s.lineno), cValue(s.cValue), iValue(s.iValue), sValue(std::string(s.sValue))
        { }
};



/**
 * @brief Lexical analysis.
 *
 * The Scanner is an abstract interface for flex generated
 * scanner.
 */
class Scanner
{
    public:
        /** Default destructor */
        virtual ~Scanner() { }

        /**
         * Aquire next token from source file.
         * @param token A place for next token.
         * @return  False in case of eof, otherwise true.
         */
        virtual bool getNextToken(Token& token) =0;


        /**
         * Open th preset source file and start scanning it.
         * @return  If it fail to open source file 1, otherwise 0.
         */
        virtual int startScanning() =0;


        /**
         * Close the source file and stop scanning.
         */
        virtual void stopScanning() =0;
};


/**
 * @brief Abstract factory for scanner.
 *
 * As far as Scanner is abstract class and the existance of FlexScanner
 * is hidden from outside we need a factory to create an instance
 * of new Scanner object.
 */
class ScannerFactory
{
	public:
        /**
         * Create new scanner.
         * @return  An instance of Scanner.
         * @warning Caller is responsible for deallocating object.
         */
		static Scanner* createScanner();

	private:
		ScannerFactory();
};

} // end namespace frontend


/** Print token type in human readable form. */
std::ostream& operator<<(std::ostream&, frontend::eTokens);

/** Print Token in human readable form. */
std::ostream& operator<<(std::ostream &, const frontend::Token &);


#endif // SCANNER_H
