#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H
#include <string>
#include <vector>
#include <map>

#include "instruction.h"



namespace symbols
{


/**
 * Specify the method how the symbol was created.
 */
enum eOrigin
{
    oUnknown,       /**< Not known or error. */
    oConstant,      /**< A constant from original source code. */
    oVariable,      /**< A programmer's variable. */
    oExpression,    /**< Created while parsing an expression, a subexpression. */
    oArgument       /**< Function argument, in fact a special case of variable. */
};


/**
 * Determine if the function or variable was defined or not.
 */
enum eUsage
{
    uUnknown,       /**< Unknown status. */
    uDeclared,      /**< Item was declared */
    uDefined,       /**< Item was not only declared but defined too. */
    uUsed           /**< Variable was used, function was called. */
};


/**
 * The block (of code) can be placed in different
 * semantic positions. For example a function body
 * is a block, a body of while loop is also o block.
 * This type denotes such kinds of block placement.
 */
enum eBlockType
{
    bGlobal,        /**< A block denoting one (whole) file. */
    bFunction,      /**< A function body. */
    bBlock          /**< A block restricted by symbols '{' and '}'. */
};


/**
 * @brief A variable or symbol.
 *
 *
 */
class Symbol : public Operand
{
   public:
        Symbol();
        Symbol(const eDataType type, const std::string name, const eOrigin origin);

        ~Symbol() {}


        std::string getName() const { return m_name; }
        void setName(const std::string name) { m_name = name; }

        eDataType getType() const { return m_type; }


        void setValue(char c) { cValue = c; }
        void setValue(int i) { iValue = i; }
        void setValue(std::string s) { sValue = s; }

        virtual std::string asString() const;
        std::string asString(const bool mini) const;

   protected:

   private:
        /* std::string m_name;  inherited from operand */
        std::string m_id;
        eOrigin m_origin;
        eDataType m_type;
        int m_offset;
        eUsage m_usage;
        int lineno;

        char cValue;
        int iValue;
        std::string sValue;


        /** Assign default values to attributes */
        void init();
};


/**
 * @brief The table of variables.
 *
 * This class holds the table for all variables.
 * However it's not a global table (do not contain all
 * variables on source file) merely only variables of
 * one block therefore it is equivalent with a block
 * of source code (in matter of describing variables of
 * cource).
 *
 * It also contain a reference to all nested blocks from
 * this one as parent.
 */
class SymbolTable
{
    public:
        /** Default constructor */
        SymbolTable(const eBlockType type, std::string name = "");

        /** Default destructor */
        virtual ~SymbolTable();

        /** Getter for type of this block. */
        eBlockType getType() const { return m_type; }


        /**
         * Add a new symbol/variable to this block.
         * @param type      The data type of new variable.
         * @param name      The name for this variable.
         * @param origin    The way how it was declared (see eOrigin).
         * @return      Return new symbol for additional configuration.
         */
        Symbol* addSymbol(const eDataType type, const std::string name, const eOrigin origin);

        /**
         * Find symbol with given namein this table.
         * @param name  Name of symbol.
         * @return      Reference to symbol or NULL if there is no symbol with that name.
         */
        Symbol* findSymbol(const std::string name);


        /**
         * Create a new block which will be a subblock of this one.
         * @param type      Type of newly created block (see eBlockType)
         * @param name      A new name for this block.
         * @return      Return newly created block for additional configuration.
         */
        SymbolTable* openNewBlock(const eBlockType type, const std::string name);

        /**
         * Add an inherited block to this one.
         * @param block     New subblock of this one.
         */
        SymbolTable* openNewBlock(SymbolTable* block);



        /**
         * Show the text representation of table.
         * @param indent        Indent text for prettier output.
         * @return      String representation.
         */
        std::string asString(const unsigned int indent = 4) const;


    protected:
    private:
        eBlockType m_type;                          /**< Block type or level of nested block */
        std::string m_name;                         /**< Identification of block */
        std::map<std::string, Symbol*> m_symbols;   /**< The actual table of all symbols/variables. */
        std::vector<SymbolTable*> m_nestedBlocks;   /**< Blocks nested from this block */

        int m_offsetGenerator;                      /**< Provide unique offset for each variable */
};

/// SymbolTable and Block are equivalent terms
typedef SymbolTable Block;

}   // end namespace symbols

/** Print data type in human readable form. */
std::ostream& operator<<(std::ostream&, symbols::eBlockType);

/** Print origin type in human readable form. */
std::ostream& operator<<(std::ostream&, symbols::eOrigin);

/** Print data type in human readable form. */
std::ostream& operator<<(std::ostream&, symbols::eDataType);

#endif // SYMBOLTABLE_H
