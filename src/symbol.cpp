#include <sstream>

#include "symbol.h"
#include "logger.h"
#include "utils.h"


using namespace std;
using namespace symbols;



Symbol::Symbol()
    : Operand()
{
    init();
}


Symbol::Symbol(const eDataType type, const std::string name, const eOrigin origin)
    : Operand()
{
    init();
    m_type = type;
    m_name = name;
    m_origin = origin;

    cValue = '\0';
    iValue = 0;
    sValue = "";
}


void Symbol::init()
{
    m_name = "";
    m_id = "";
    m_origin = oUnknown;
    m_type = tUnset;
    m_offset = 0;
    m_usage = uUnknown;
    lineno = 0;
}


string Symbol::asString() const
{
    stringstream ss;

    ss << m_name;

    return ss.str();
}


string Symbol::asString(const bool mini) const
{
    stringstream ss;

    ss << m_type << " " << m_name;

    if( mini )
        return ss.str();

    ss << " = ";

    switch(m_type)
    {
        case tChar:
            ss << "'" << cValue << "'";
            break;
        case tInt:
            ss << iValue;
            break;
        case tString:
            ss << "\"" << sValue << "\"";
            break;
        default:
            WARNING("Unknown type.")
    }

    ss << " { " << m_origin << " };";

    return ss.str();
}



//
//      Symbol table
//

SymbolTable::SymbolTable(const eBlockType type, const string name)
    : m_type(type), m_name(name)
{
    //ctor
}


SymbolTable::~SymbolTable()
{
    for(vector<SymbolTable*>::iterator it = m_nestedBlocks.begin(); it != m_nestedBlocks.end(); it++)
        delete *it;

    m_nestedBlocks.clear();

    for( map<string, Symbol*>::iterator it = m_symbols.begin(); it != m_symbols.end(); it++)
        delete it->second;

    m_symbols.clear();
}


Symbol* SymbolTable::addSymbol(const eDataType type, const string name, const eOrigin origin)
{
    INFO("Block \"" << m_name << "\" added a symbol: " << type << " " << name)
    Symbol* tmpSymbol = NULL;

    if( findSymbol(name) == NULL )
    {
        tmpSymbol = new Symbol(type, name, origin);
        m_symbols[name] = tmpSymbol;
    }
    else if( name == "" )
    {
        DEBUG("Variable collision - I hope this is argument in declaration.")
    }
    else
        WARNING("Variable already declared.")

    return tmpSymbol;
}


Symbol* SymbolTable::findSymbol(const string name)
{
    if( m_symbols.find(name) != m_symbols.end() )
        return m_symbols[name];

    return NULL;
}


SymbolTable* SymbolTable::openNewBlock(const eBlockType type, const std::string name)
{
    SymbolTable* tmpBlock = new SymbolTable(type, name);
    m_nestedBlocks.push_back(tmpBlock);

    return tmpBlock;
}


SymbolTable* SymbolTable::openNewBlock(SymbolTable* block)
{
    m_nestedBlocks.push_back(block);

    return block;
}


string SymbolTable::asString(const unsigned int indent) const
{
    stringstream ss;

    ss << getIndent(indent) << m_type << "#" << m_name << ":" << endl;

    for( map<string, Symbol*>::const_iterator it = m_symbols.begin(); it != m_symbols.end(); it++)
        ss << getIndent(indent+1) << "-> "<< it->second->asString(false) << endl;

    for(vector<SymbolTable*>::const_iterator it = m_nestedBlocks.begin(); it != m_nestedBlocks.end(); it++)
        ss << (*it)->asString(indent+2) << endl;

    return ss.str();
}


//
//      Enumerations stream outputs
//


ostream& operator<<(ostream &os, eBlockType type)
{
    switch(type)
    {
    	case bGlobal:
            os << "global";
    		break;
        case bFunction:
            os << "function";
            break;
        case bBlock:
            os << "basic-block";
            break;
    	default:
            WARNING("Unknown block type.")
    }

    return os;
}


ostream& operator<<(ostream &os, eOrigin orig)
{
    switch(orig)
    {
        case oUnknown:
            os << "<not set>";
            break;
        case oConstant:
            os << "constant";
            break;
        case oVariable:
            os << "variable";
            break;
        case oExpression:
            os << "expression";
            break;
        case oArgument:
            os << "argument";
            break;
        default:
            WARNING("Unknown orogin type.")
    }

    return os;
}


ostream& operator<<(ostream &os, eDataType type)
{
    switch(type)
    {
        case tUnset:
            os << "<not set>";
            break;
    	case tVoid:
            os << "void";
    		break;
        case tChar:
            os << "char";
            break;
        case tInt:
            os << "int";
            break;
        case tString:
            os << "string";
            break;
    	default:
            WARNING("Unknown data type.")
    }

    return os;
}
