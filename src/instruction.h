#ifndef INSTRUCTION_H
#define INSTRUCTION_H
#include <cstdlib>
#include <vector>
#include <string>


namespace symbols
{



/**
 * Types of instructions in our 3AC representation.
 */
enum eOpType
{
    iNop,           /**< empty instruction */
    iHalt,          /**< halt the processor */
    iCall,          /**< function call */
    iReturn,        /**< return from function */

    iVar,           /**< allocate new variable */
    iArg,           /**< allocate function argument */
    iFun,           /**< allocate space for return value */

    iAssign,        /**< assigment */
    iAdd,           /**< addition */
    iSub,           /**< subtraction */
    iMult,          /**< multiplication */
    iDiv,           /**< division */
    iMod,           /**< remainder */

    iGoto,          /**< unconditional jump */
    iBrIfNot,       /**< branch if condition is not true */

    iCast,          /**< type-cast */

    iLabel          /**< a jump destination (mostly just NOOP instruction) */
};


/**
 * Instruction's data types.
 */
enum eDataType
{
    tUnset,         /**< unknown data type used for detecting errors */
    tVoid,          /**< not really a data type but void */
    tChar,          /**< a character */
    tInt,           /**< a integer number */
    tString         /**< a string */
    // NOTE (nilbeleth#1#): Add new types if necessary (like tType2Type)
};


/**
 * @brief An operand for instruction.
 *
 */
class Operand
{
    public:
        Operand();

        virtual ~Operand() {}

        virtual std::string asString() const =0;

    protected:
        std::string m_name;         /**< Some sort of identification (doesn't to be unique). */
};


/**
 * @brief An IR (or 3AC) instruction.
 *
 */
class Instruction : public Operand
{
    public:
        /** Default constructor */
        Instruction();

        /** A construcor with arguments */
        Instruction(const eOpType instr, const eDataType type, const Operand* res, const Operand* op1 = NULL, const Operand* op2 = NULL);

        /** Default destructor */
        virtual ~Instruction();

        virtual std::string asString() const;


    private:
        eOpType m_instr;            /**< The type of instruction (e.g. addition, jump, nop,...). */
        eDataType m_type;           /**< On which type of operands and result the instruction works? */
        const Operand* m_result;    /**< The result of instruction. */
        const Operand* m_operand1;  /**< The first operand. */
        const Operand* m_operand2;  /**< The second operand. */
};

}   // end namespace symbols


/** Print data type in human readable form. */
std::ostream& operator<<(std::ostream&, symbols::eOpType);

#endif // INSTRUCTION_H
