#ifndef SETTINGS_H
#define SETTINGS_H
#include <string>



/**
 * @brief Parser for program's arguments.
 *
 * Class Settings takes arguments from commandline
 * and parses them. In addition it is responsible
 * for providing the settings (aquired from command line)
 * to other parts of program.
 */
class Settings
{
    public:
        /** Aquire the instance of Settings */
        static Settings &getInstance();


        /** Default destructor */
        ~Settings();


        /**
         * Parse comandline arguments and ensures
         * all options has been set.
         * @param argc  Number of commandline strings.
         * @param argv  Array of strings from commandline.
         * @return  If all is well return 0 else 1.
         */
        int parseArgs(int argc, char* argv[]);


        /**
         * Set a new source file for compiling.
         * @note    Not used as far project description states only one file should be parsed.
         * @param filename      Name of the new source file.
         */
        void setNewInputFile(const std::string filename);


        /** Print help. */
        void printHelp() const;


        /** Get verbosity. */
        int getVerbosity() const { return m_bVerbose; }


        /** Get debug verbosity level. */
        bool getDebug() const { return m_bDebug; }


        /** Get optimalization level. */
        int getOptimLevel() const { return m_iOptimLevel; }


        /** Get source filename. */
        std::string getInputFile() const { return m_sInput; }


        /** Get output filename. */
        std::string getOutputFile() const { return m_sOutput; }


    private:
        Settings();
        Settings(const Settings &);
        Settings &operator=(const Settings &s);

        int m_bVerbose;             /**< Print additional information according to verbose level. */
        bool m_bDebug;              /**< Print debugging information as well. */
        int m_iOptimLevel;          /**< Set optimalizations level. (0 - none, 2 - all) */
        std::string m_sInput;       /**< Source file for compilator. */
        std::string m_sOutput;      /**< Write output program in this file. */
};

#endif // SETTINGS_H
