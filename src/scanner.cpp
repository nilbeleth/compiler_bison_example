/*/===========================================================================\*
|*|         "VYPe 2012: C-based language to MIPS32 compiler"                  |*
|*|===========================================================================|*
|*|  Author: Matej Odalos   (xodalo00@fit.vutbr.cz)                           |*
|*|          Ondrej Polesny (xpoles00@fit.vutbr.cz)                           |*
|*|---------------------------------------------------------------------------|*
|*|  Info: Lexical analysis of source program                                 |*
|*|---------------------------------------------------------------------------|*
|*| VERSION   DATE    TIME  BY        CHANGE/COMMENTS                         |*
|*|---------------------------------------------------------------------------|*
|*|   0.01  04/10/12  16:30 xodalo00  Created                                 |*
\*\===========================================================================|*/
#include <FlexLexer.h>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <map>

#include "scanner.h"
#include "logger.h"
#include "debug.h"


using namespace frontend;
using namespace std;



namespace frontend
{

/** Map all keywords and built-in functions from strings to tokens */
typedef map<string, eTokens> tKWMap;



/** \internal
 * @brief Wrapper for flex generated class.
 *
 * PrivateFlexScanner is a wrapper around actual flex scanner class.
 * Main purpose is to hide some of flex class functions and produce
 * desired output (in opposition to flex predefined output).
 * @warning This class is not visible outside this modul.
 */
class PrivateFlexLexer : public yyFlexLexer
{
    public:
        /** */
        PrivateFlexLexer(istream &is, string filename)
            : yyFlexLexer(&is, &cerr), m_filename(filename), m_error(false)
        { }

        /** Just type-cast output of flex to our eTokens. */
        eTokens readNext() { return static_cast<eTokens>(this->yylex()); }

    protected:
        virtual void LexerOutput(const char* buf, int size);
        virtual void LexerError(const char* msg);

    private:
        string m_filename;          /// ???
        bool m_error;               /// ???
};



/** \internal
 * @brief Decorator class binding flex scanner and application.
 *
 * The FlexScanner uses flex generated scanner for lexical analysis
 * of the source file. In addition it enhances the functionality by
 * identifying keywords and built-in functions over identifiers and
 * interpreting identifiers, numbers, characters and strings value.
 * A decorator pattern design was used for this class.
 *
 * @warning This class is not visible outside this modul.
 */
class FlexScanner : public Scanner
{
    public:
        /** Default constructor */
        FlexScanner();

        /** Default destructor */
        virtual ~FlexScanner();


        /**
         *
         */
        virtual bool getNextToken(Token &token);

        virtual int startScanning();

        virtual void stopScanning();

    protected:

    private:
        string m_filename;                      /**< Source file's name. */
        fstream m_fsInput;                      /**< Input stream */
        PrivateFlexLexer* m_flexAnalyzer;       /**< The flex generated engine for lexical analysis. */
        tKWMap m_kwMap;                         /**< Which identifiers are not identifiers but keywords or built-in functions? */
        bool m_scanning;                        /**< Are we in process of scanning right now? */

        int readNumber(const string s);
        char readChar(const string s);
        string readString(const string s);
};

}   // namespace frontend



void PrivateFlexLexer::LexerOutput(const char* buf, int size)
{
    string msg(buf, size);
    Logger& logger = Logger::getInstance();

    if( msg == "'" )
        logger.logError(eLexical, "bad character.", yylineno);
    else
        logger.logError(eLexical, "lexem not recognized.", yylineno);
}


void PrivateFlexLexer::LexerError(const char* msg)
{
    WARNING("Flex error occured: " << msg)
}


FlexScanner::FlexScanner()
    : m_fsInput(NULL), m_scanning(false)
{
    // obtain file name
    m_filename = Settings::getInstance().getInputFile();

    // init all keywords
    m_kwMap.clear();
    m_kwMap["break"]        = T_KW_BREAK;
    m_kwMap["char"]         = T_KW_CHAR;
    m_kwMap["continue"]     = T_KW_CONTINUE;
    m_kwMap["else"]         = T_KW_ELSE;
    m_kwMap["for"]          = T_KW_FOR;
    m_kwMap["if"]           = T_KW_IF;
    m_kwMap["int"]          = T_KW_INT;
    m_kwMap["return"]       = T_KW_RETURN;
    m_kwMap["short"]        = T_KW_SHORT;
    m_kwMap["string"]       = T_KW_STRING;
    m_kwMap["unsigned"]     = T_KW_UNSIGNED;
    m_kwMap["void"]         = T_KW_VOID;
    m_kwMap["while"]        = T_KW_WHILE;
    m_kwMap["get_at"]       = T_FN_GET_AT;
    m_kwMap["print"]        = T_FN_PRINT;
    m_kwMap["read_char"]    = T_FN_READ_CHAR;
    m_kwMap["read_int"]     = T_FN_READ_INT;
    m_kwMap["read_string"]  = T_FN_READ_STRING;
    m_kwMap["set_at"]       = T_FN_SET_AT;
    m_kwMap["strcat"]       = T_FN_STRCAT;


    // init flex analyzer
    m_flexAnalyzer = new PrivateFlexLexer(m_fsInput, m_filename);
    if( !m_flexAnalyzer )
        ERROR("Unable to obtain flex scanner instance.")
}


FlexScanner::~FlexScanner()
{
    delete m_flexAnalyzer;
    m_flexAnalyzer = NULL;
}


//
bool FlexScanner::getNextToken(Token &token)
{
    eTokens type;
    if( !(type = m_flexAnalyzer->readNext()) )
        return false;

    token.type = type;
    token.lineno = m_flexAnalyzer->lineno();
    string text = m_flexAnalyzer->YYText();

    switch(type)
    {
        case T_ID:
        {
            // match with table of keywords
            tKWMap::const_iterator it;

            it = m_kwMap.find(text);
            if( it != m_kwMap.end() )
            {
                token.type = it->second;
                break;
            }

            // no keyword => get idetifier name
            token.sValue = text;

            break;
        }
        case T_NUM:
            // read string
            token.iValue = readNumber(text);
            break;
        case T_CHAR:
            // read string
            token.cValue = readChar(text);
            break;
        case T_STRING:
            // read string
            token.sValue = readString(text);
            break;
        default:
            break;
    }

    return true;
}


int FlexScanner::startScanning()
{
    m_scanning = true;

    // safely open file
    m_fsInput.open(m_filename.c_str(), ios::in);
    if( !m_fsInput )
    {
        Logger::getInstance().logError(eInternal, "can't open file \"" + m_filename + "\"");
        return 1;
    }

    return 0;
}


void FlexScanner::stopScanning()
{
    m_fsInput.close();

    m_scanning = false;
}


char FlexScanner::readChar(const string s)
{
    char c = s.at(1);

    if( c == '\\' )
    {
        switch( s.at(2) )
        {
            case 'n':
                c = '\n';
                break;
            case 't':
                c = '\t';
                break;
            case '\\':
                c = '\\';
                break;
            case '\'':
                c = '\'';
                break;
            case '"':
                c = '"';
                break;
            default:
                Logger::getInstance().logError(eLexical, "escape sequance not permitted.", m_flexAnalyzer->lineno());
        }
    }

    return c;
}


int FlexScanner::readNumber(const string s)
{
    int n = atoi(s.c_str());

    return n;
}


string FlexScanner::readString(const string s)
{
    string str = s.substr(1, s.size() -2);
    size_t pos = string::npos;

    // convert \n to actual newline
    while( (pos = str.find("\\n")) != string::npos )
        str.replace(pos, 2, "\n");

    // convert \t to actual tab
    while( (pos = str.find("\\t")) != string::npos )
        str.replace(pos, 2, "\t");

    // convert \\ to just one '\'
    while( (pos = str.find("\\\\")) != string::npos )
        str.replace(pos, 2, "\\");

    // convert \" to just "
    while( (pos = str.find("\\\"")) != string::npos )
        str.replace(pos, 2, "\"");

    // convert \' to just '
    while( (pos = str.find("\\'")) != string::npos )
        str.replace(pos, 2, "'");

    if( string::npos != str.find("\\"))
        WARNING("Found backslash where there should not be any")

    return str;
}


Scanner* ScannerFactory::createScanner()
{
    return new FlexScanner();
}

// pretty print token type
ostream& operator<<(ostream &os, eTokens type)
{
    switch(type)
    {
        case T_NULL:
            os << "NULL";
            break;
        case T_ID:
            os << "identifier";
            break;
        case T_NUM:
            os << "number";
            break;
        case T_CHAR:
            os << "character";
            break;
        case T_STRING:
            os << "string";
            break;
        case T_OP_LPAR:
        case T_OP_RPAR:
        case T_OP_LBRA:
        case T_OP_RBRA:
        case T_OP_ASSIGN:
        case T_OP_PLUS:
        case T_OP_MINUS:
        case T_OP_TIMES:
        case T_OP_DIVIDE:
        case T_OP_MODULO:
        case T_OP_GREATER:
        case T_OP_GREATER_EQ:
        case T_OP_LESS:
        case T_OP_LESS_EQ:
        case T_OP_EQUAL:
        case T_OP_NONEQUAL:
        case T_OP_NEG:
        case T_OP_OR:
        case T_OP_AND:
        case T_OP_COLON:
        case T_OP_SEMICOLON:
            os << "operator ";
            break;
        case T_KW_BREAK:
        case T_KW_CHAR:
        case T_KW_CONTINUE:
        case T_KW_ELSE:
        case T_KW_FOR:
        case T_KW_IF:
        case T_KW_INT:
        case T_KW_RETURN:
        case T_KW_SHORT:
        case T_KW_STRING:
        case T_KW_UNSIGNED:
        case T_KW_VOID:
        case T_KW_WHILE:
            os << "keyword ";
            break;
        case T_FN_PRINT:
        case T_FN_READ_CHAR:
        case T_FN_READ_INT:
        case T_FN_READ_STRING:
        case T_FN_GET_AT:
        case T_FN_SET_AT:
        case T_FN_STRCAT:
            os << "function ";
            break;
        default:
            WARNING("Unknown token: " << static_cast<int>(type))
            break;
    }

    switch(type)
    {
        case T_OP_LPAR:
            os << "\"(\"";
            break;
        case T_OP_RPAR:
            os << "\")\"";
            break;
        case T_OP_LBRA:
            os << "\"{\"";
            break;
        case T_OP_RBRA:
            os << "\"}\"";
            break;
        case T_OP_ASSIGN:
            os << "\"=\"";
            break;
        case T_OP_PLUS:
            os << "\"+\"";
            break;
        case T_OP_MINUS:
            os << "\"-\"";
            break;
        case T_OP_TIMES:
            os << "\"*\"";
            break;
        case T_OP_DIVIDE:
            os << "\"/\"";
            break;
        case T_OP_MODULO:
            os << "\"%\"";
            break;
        case T_OP_GREATER:
            os << "\">\"";
            break;
        case T_OP_GREATER_EQ:
            os << "\">=\"";
            break;
        case T_OP_LESS:
            os << "\"<\"";
            break;
        case T_OP_LESS_EQ:
            os << "\"<=\"";
            break;
        case T_OP_EQUAL:
            os << "\"==\"";
            break;
        case T_OP_NONEQUAL:
            os << "\"!=\"";
            break;
        case T_OP_NEG:
            os << "\"!\"";
            break;
        case T_OP_OR:
            os << "\"||\"";
            break;
        case T_OP_AND:
            os << "\"&&\"";
            break;
        case T_OP_COLON:
            os << "\",\"";
            break;
        case T_OP_SEMICOLON:
            os << "\";\"";
            break;
        case T_KW_BREAK:
            os << "\"break\"";
            break;
        case T_KW_CHAR:
            os << "\"char\"";
            break;
        case T_KW_CONTINUE:
            os << "\"continue\"";
            break;
        case T_KW_ELSE:
            os << "\"else\"";
            break;
        case T_KW_FOR:
            os << "\"for\"";
            break;
        case T_KW_IF:
            os << "\"if\"";
            break;
        case T_KW_INT:
            os << "\"int\"";
            break;
        case T_KW_RETURN:
            os << "\"return\"";
            break;
        case T_KW_SHORT:
            os << "\"short\"";
            break;
        case T_KW_STRING:
            os << "\"string\"";
            break;
        case T_KW_UNSIGNED:
            os << "\"unsigned\"";
            break;
        case T_KW_VOID:
            os << "\"void\"";
            break;
        case T_KW_WHILE:
            os << "\"while\"";
            break;
        case T_FN_PRINT:
            os << "print()";
            break;
        case T_FN_READ_CHAR:
            os << "read_char()";
            break;
        case T_FN_READ_INT:
            os << "read_int()";
            break;
        case T_FN_READ_STRING:
            os << "read_string()";
            break;
        case T_FN_GET_AT:
            os << "get_at()";
            break;
        case T_FN_SET_AT:
            os << "set_at()";
            break;
        case T_FN_STRCAT:
            os << "strcat()";
            break;
        default:
            break;
    }

    return os;
}


// pretty print token
ostream& operator<<(ostream &os, const Token &token)
{
    os << token.type << "@" << token.lineno;

    switch(token.type)
    {
    	case T_ID:
            os << " (" << token.sValue << ")";
            break;
    	case T_NUM:
            os << " (" << token.iValue << ")";
            break;
    	case T_CHAR:
    	    os << " ('" << token.cValue << "')";
            break;
    	case T_STRING:
    	    os << " (\"" << token.sValue << "\")";
            break;
    	default:
            break;
    }

    return os;
}
