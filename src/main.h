#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED
/**
 * @mainpage VYPe12 project
 *
 * VYPe12 is a compiler from C derivated language to MIPS32 assambly languge.
 * This project was created as a homework assigment for subject VYPe (Compiler construction) at Faculty of Information Technologies, University of technology in Brno.
 * In this short article we will show the main principles on which is out compiler built.
 *
 * @tableofcontents
 * @section frontend Front-end
 * The main purpose of front-end is mostly to parse the source file
 *
 * @subsection scanner Lexical analysis
 * Hugi buky saa tralala dhdkjhdkjahd ldkjaldj
 *
 *
 * @subsection parser Syntactic and semantic analysis
 * Dslkdjsld dhiodjad djalsjd nxcnaiod ndxjasdlk; nd;lakdj
 *
 *
 * @subsection ir Intermediate representation
 * Dkjal;kdka kjdhdhdk duhjasdh kdjhakjdhh nxjasiudhd dakljd
 *
 *
 *
 * @section backend Back-end
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Fusce sollicitudin dignissim feugiat.
 * In hac habitasse platea dictumst.
 * Nunc porttitor ipsum sit amet neque lobortis ac bibendum purus sollicitudin.
 * Donec eleifend fringilla lectus, a auctor elit facilisis ac.
 * Ut in nisl vel purus tincidunt interdum eu et mauris.
 * Praesent sem dolor, fringilla nec aliquam quis, accumsan sed libero.
 * Duis feugiat felis id dolor cursus et convallis nunc semper.
 * Pellentesque suscipit metus eget elit aliquam sit amet porttitor leo lobortis.
 * Integer sodales orci at nisi aliquam ac volutpat nulla scelerisque.
 * Nulla pulvinar neque non metus mollis et elementum nunc eleifend.
 * In eros quam, porttitor ut congue eu, sodales in enim.
 * Sed mauris dui, vehicula eget dictum ullamcorper, pharetra vel mi.
 * Maecenas vitae dui risus, nec varius orci.
 * Pellentesque nec velit augue, nec ullamcorper neque.
 * Vestibulum accumsan varius nulla, quis blandit orci tincidunt vitae.
 * Nunc quis mi lorem, in sagittis libero.
 *
 * Mauris fermentum malesuada fermentum.
 * Donec mi massa, tincidunt vitae varius eu, interdum a massa.
 * Nunc consectetur tellus eu magna iaculis feugiat.
 * Duis lacus dolor, venenatis fringilla feugiat non, pharetra in nibh.
 * Phasellus tempor mollis turpis, et consectetur risus hendrerit quis.
 * Nulla facilisi.
 * Vestibulum at neque in lectus placerat porta.
 * Nulla neque lorem, commodo quis volutpat molestie, auctor nec sapien.
 * Maecenas consectetur vulputate lectus sit amet fringilla.
 * Maecenas molestie euismod leo.
 * Curabitur fringilla lobortis lacus, nec porttitor dolor cursus non.
 * Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
 */

/**
 * @file    main.h
 *
 * @brief   Entrypoint of application.
 *
 * @authors Matej Odalos, xodalo00@stud.fit.vutbr.cz \n
 *          Ondrej Polesny, xpoles01@stud.fit.vutbr.cz
 *
 * @date    November, 1st 2012 - created
 * @date    November, 2nd 2012 - added main page for doxy documentation
 * @date    02.11.2012 - modified
 */


// TODO (nilbeleth#1#): erase all beneath
/*! \page page1 A documentation page
  \tableofcontents
  Leading text.
  \section sec An example section
  This page contains the subsections \ref subsection1 and \ref subsection2.
  For more info see page \ref page2.
  \subsection subsection1 The first subsection
  Text.
  \subsection subsection2 The second subsection
  More text.
*/

/*! \page page2 Another page
  Even more info.
*/
#endif // MAIN_H_INCLUDED
