/*** prologue ***/

%{ /* C/C++ Declarations */
#include "logger.h"
#include "scanner.h"
#include "irgenerator.h"
#include <iostream>	// TODO: zmazat !!!!

#undef YYSTYPE
#define YYSTYPE frontend::Token

// hard fix for yylex's types
#include "bison_parser.hpp"
%}

/*** yacc/bison Declarations ***/
%skeleton "lalr1.cc"
%output "bison_parser.cpp"
%language "c++"
%debug
%error-verbose
%locations
%defines
%define parser_class_name "BisonParser"
%define namespace "frontend"

/* additional arguments for parser */
%parse-param { Scanner* scanner }
%parse-param { IRGenerator* gen }
%lex-param { Scanner* scanner }


/*** grammar declarations ***/
%start PROG

/* tokens */
%token T_ID T_INT T_CHAR T_STRING
%token '(' ')' '{' '}'
%token '=' ';' ','
%token '+' '-' '*' '/' '%'
%token '>' T_GR_EQ '<' T_LE_EQ T_EQ T_NEQ
%token '!' T_OR T_AND
%token KW_BREAK KW_CHAR KW_CONTINUE KW_ELSE KW_FOR KW_IF KW_INT KW_RETURN KW_SHORT KW_STRING KW_UNSIGNED KW_VOID KW_WHILE
%token FN_PRINT FN_READ_CHAR FN_READ_INT FN_READ_STRING FN_GET_AT FN_SET_AT FN_STRCAT

/* precendence */
%left T_OR
%left T_AND
%left T_EQ T_NEQ
%left '<' '>' T_GR_EQ T_LE_EQ
%left '+' '-'
%left '*' '/' '%'
%left '!'
%left '(' ')'


%{
namespace frontend
{
	// scanner binding
	int yylex(Token*, location*, Scanner*);
}

%}

%% /*** GRAMMAR RULES ***/
/* first declarations of functions possibly global variables */
PROG		: DECL_LIST
;
DECL_LIST	: DECL DECL_LIST
		| DECL
;
DECL		: FNC
		| VAR
;

DECL_INIT	: TYPE T_ID					{ gen->saveID($2); }
;


FNC		: FNC_INIT KW_VOID ')' FNC_BODY
		| FNC_INIT ARGs ')' FNC_BODY
		| FNC_INIT KW_VOID ')' ';'			{ gen->setFuncDeclared(); }
		| FNC_INIT TYPEs ')' ';'			{ gen->setFuncDeclared(); }
;
FNC_INIT	: KW_VOID T_ID '('				{ gen->setType(symbols::tVoid); gen->selectFunct($2); }
		| DECL_INIT '('					{ gen->selectFunct($2); }
;
FNC_BODY	: FNC_START STMTs FNC_END
;
FNC_START	: '{'						{ gen->enterFunct(); }
;
FNC_END		: '}'						{ gen->leaveFunct(); }
;

ARGs		: ARG
		| ARG ',' ARGs
;
ARG		: TYPE T_ID					{ gen->addArg($2); }
;
TYPEs		: TYPE						{ gen->declArg(); }
		| TYPEs ',' TYPE				{ gen->declArg(); }
;


VAR		: DECL_VAR ';'					{ gen->unsetType(); }
;
DECL_VAR	: FIRST
		| FIRST ',' IDs
;
FIRST		: DECL_INIT					{ gen->declareVar($$); }
		| DECL_INIT '=' CONST				{ gen->declareVar($2,$3); }
;
IDs		: ID
		| ID ',' IDs
;
ID		: T_ID						{ gen->declareVar($1); }
		| T_ID '=' CONST				{ gen->declareVar($2,$3); }




STMTs		: /* epsilon */
		| STMT STMTs					{ std::cout << "statment." << std::endl; }
;


STMT		: ASSIGN_STMT
		| BLOCK_STMT
		/*| CALL_STMT */
		| EXPR_STMT
		/*| FOR_STMT*/
		| IF_STMT
		| JUMP_STMT					{ /* TODO: nemal by som to dako obmedzit len do bloku? */ }
		| RETURN_STMT
		| VAR_STMT
		/*| WHILE_STMT*/
;


ASSIGN_STMT	: T_ID '=' EXPR ';'				{ gen->assign($1); }
		/*| T_ID '=' CALL_STMT*/
;

BLOCK_STMT	: BLOCK_INIT STMTs BLOCK_END
;
BLOCK_INIT	: '{'
;
BLOCK_END	: '}'
;

EXPR_STMT	: EXPR ';'
		| ';'
;

IF_STMT		: IF_INIT COND IF_TRUE STMT IF_FALSE STMT	/* TODO: vyhod najvrchnejsiu if strukturu */
;
IF_INIT		: KW_IF '('					{ gen->enterIfCond(); }
;
IF_TRUE		: ')'						{ gen->enterIfTrue(); }
;
IF_FALSE	: KW_ELSE					{ gen->enterIfFalse(); }
;

JUMP_STMT	: KW_BREAK ';'
		| KW_CONTINUE ';'
;

RETURN_STMT	: KW_RETURN ';'					{ gen->ret(); }
		| KW_RETURN EXPR ';'				{ gen->ret(true); }
;

VAR_STMT	: DECL_VAR ';'					{ gen->unsetType(); }
;


/* conditions */
COND		: EXPR T_OR COND
		| EXPR T_AND COND
		/*| '!' COND*/
		| EXPR
;



/* expressions */
EXPR		: '(' EXPR ')'
		| EXPR '*' EXPR					{ gen->evalBinOp($2); }
		| EXPR '/' EXPR					{ gen->evalBinOp($2); }
		| EXPR '%' EXPR					{ gen->evalBinOp($2); }
		| EXPR '+' EXPR					{ gen->evalBinOp($2); }
		| EXPR '-' EXPR					{ gen->evalBinOp($2); }
		| EXPR '<' EXPR
		| EXPR '>' EXPR
		| EXPR T_GR_EQ EXPR
		| EXPR T_LE_EQ EXPR
		| EXPR T_EQ EXPR
		| EXPR T_NEQ EXPR
		| '!' EXPR					{ gen->evalUnaOp($1); }
		| T_ID						{ gen->loadSymbol($1); }
		| CONST
;


/* suporting nonterminals */
TYPE		: KW_INT					{ gen->setType(symbols::tInt); }
		| KW_CHAR					{ gen->setType(symbols::tChar); }
		| KW_STRING					{ gen->setType(symbols::tString); }
;

CONST		: T_INT						{ gen->defConst($1); }
		| T_CHAR					{ gen->defConst($1); }
		| T_STRING					{ gen->defConst($1); }
;

/*** end grammar rules ***/
%%


using namespace frontend;


int frontend::yylex(Token* token, location* loc, Scanner* scanner)
{

	while( scanner->getNextToken(*token) )
	{
		loc->begin.line = loc->end.line = token->lineno;

		switch(token->type)
		{
			case T_ID: 		return BisonParser::token::T_ID;
			case T_NUM:		return BisonParser::token::T_INT;
			case T_CHAR:		return BisonParser::token::T_CHAR;
			case T_STRING:		return BisonParser::token::T_STRING;

			case T_OP_LPAR:		return '(';
			case T_OP_RPAR:		return ')';
			case T_OP_LBRA:		return '{';
			case T_OP_RBRA:		return '}';

			case T_OP_PLUS:		return '+';
			case T_OP_MINUS:	return '-';
			case T_OP_TIMES:	return '*';
			case T_OP_DIVIDE:	return '/';
			case T_OP_MODULO:	return '%';

			case T_OP_GREATER:	return '>';
			case T_OP_GREATER_EQ:	return BisonParser::token::T_GR_EQ;
			case T_OP_LESS:		return '<';
			case T_OP_LESS_EQ:	return BisonParser::token::T_LE_EQ;
			case T_OP_EQUAL:	return BisonParser::token::T_EQ;
			case T_OP_NONEQUAL:	return BisonParser::token::T_NEQ;

			case T_OP_NEG:		return '!';
			case T_OP_OR:		return BisonParser::token::T_OR;
			case T_OP_AND:		return BisonParser::token::T_AND;

			case T_OP_ASSIGN:	return '=';
			case T_OP_COLON:	return ',';
			case T_OP_SEMICOLON:	return ';';

			case T_KW_BREAK:	return BisonParser::token::KW_BREAK;
			case T_KW_CHAR:		return BisonParser::token::KW_CHAR;
			case T_KW_CONTINUE:	return BisonParser::token::KW_CONTINUE;
			case T_KW_ELSE:		return BisonParser::token::KW_ELSE;
			case T_KW_FOR:		return BisonParser::token::KW_FOR;
			case T_KW_IF:		return BisonParser::token::KW_IF;
			case T_KW_INT:		return BisonParser::token::KW_INT;
			case T_KW_RETURN:	return BisonParser::token::KW_RETURN;
			case T_KW_SHORT:	return BisonParser::token::KW_SHORT;
			case T_KW_STRING:	return BisonParser::token::KW_STRING;
			case T_KW_UNSIGNED:	return BisonParser::token::KW_UNSIGNED;
			case T_KW_VOID:		return BisonParser::token::KW_VOID;
			case T_KW_WHILE:	return BisonParser::token::KW_WHILE;

			case T_FN_PRINT:	return BisonParser::token::FN_PRINT;
			case T_FN_READ_CHAR:	return BisonParser::token::FN_READ_CHAR;
			case T_FN_READ_INT:	return BisonParser::token::FN_READ_INT;
			case T_FN_READ_STRING:	return BisonParser::token::FN_READ_STRING;
			case T_FN_GET_AT:	return BisonParser::token::FN_GET_AT;
			case T_FN_SET_AT:	return BisonParser::token::FN_SET_AT;
			case T_FN_STRCAT:	return BisonParser::token::FN_STRCAT;

			case T_NULL:
			default:
				WARNING("Unknown token type: " << token->type )
		}
	}


	return 0;
}


void BisonParser::error(const location& loc, const std::string& msg)
{
	WARNING("Not implemented.")
	std::cerr << loc.end.line << ":" << loc.end.column << ": " << msg << std::endl;
}
