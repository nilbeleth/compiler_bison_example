#ifndef CONTROLSTRUCTURE_H
#define CONTROLSTRUCTURE_H
#include "instruction.h"



namespace symbols
{


/**
 * @brief Control flow statement.
 *
 * Class ControlStructure represents all statements
 * that can manipulate control flow of compiled
 * program.
 */
class ControlStructure
{
    public:
        /** Default constructor */
        ControlStructure();
        /** Default destructor */
        virtual ~ControlStructure();

        Instruction* getStart();
        Instruction* getEnd();

    protected:
        Instruction* m_start;
        Instruction* m_end;

    private:
};


/**
 * @brief If statement.
 *
 *
 */
class IfStructure : public ControlStructure
{
    public:
        /** Default constructor */
        IfStructure();


    private:
};


/**
 * @brief While statement.
 *
 *
 */
class WhileStructure : public ControlStructure
{
    public:
        /** Default constructor */
        WhileStructure();

    private:
};

}   // end namespace symbols

#endif // CONTROLSTRUCTURE_H
