#include <iostream>

#include "parser.h"
#include "logger.h"
#include "settings.h"

#include "bison_parser.hpp"


using namespace std;
using namespace symbols;
using namespace frontend;



Parser::Parser()
{
    m_scanner = ScannerFactory::createScanner();
    m_generator = NULL;
    WARNING("Do not use this constructor.")
}


Parser::Parser(FileTable* table)
{
    m_scanner = ScannerFactory::createScanner();
    m_generator = new IRGenerator(table);
}


Parser::~Parser()
{
    delete m_scanner;
    delete m_generator;
}


int Parser::parse()
{
    BisonParser* parser = new BisonParser(m_scanner, m_generator);


    m_scanner->startScanning();

    m_generator->addFile(Settings::getInstance().getInputFile());
    if( Settings::getInstance().getVerbosity() >= 3 )
        parser->set_debug_level(1);
    if( 0 != parser->parse() )
        cout << "Scheisse !!!" << endl;
    m_generator->closeFile();

    m_scanner->stopScanning();


    delete parser;

    return 0;
}
