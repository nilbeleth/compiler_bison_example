#ifndef PARSER_H
#define PARSER_H
#include "scanner.h"
#include "irgenerator.h"
#include "symbol.h"



namespace frontend
{


/**
 * @brief Syntactic and semantic analysis.
 *
 * Parser used a Bison generated LALR(1) parser to parse
 * the source file, check correct syntax and semantic
 * conditions and populate SymbolTable.
 */
class Parser
{
    public:
        /** Default constructor */
        Parser();

        /** Construct parser with symbol table reference */
        Parser( symbols::FileTable* table );

        /** Default destructor */
        ~Parser();


        /**
         * Parser the source file till eof.
         * @return 1 if error occured otherwise 0.
         */
        int parse();

    private:
        Scanner* m_scanner;             /**< A lexical analyzer */
        IRGenerator* m_generator;       /**< A generator of IR code */

};

}   // end namespace frontend

#endif // PARSER_H
