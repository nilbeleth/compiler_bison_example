#include <sstream>
#include <fstream>

#include "function.h"
#include "logger.h"
#include "utils.h"


using namespace std;
using namespace symbols;



Function::Function(const std::string name, const eDataType type)
    : m_name(name), m_usage(uUnknown), m_functRootBlock(NULL), m_retType(type), /*m_offsetGenerator(0),*/  m_called(false)
{
    m_functRootBlock = new Block(bFunction, m_name);
}

Function::~Function()
{
    // precisti argument list (deallokuje sa az pri mazani prislusneho bloku)
    m_args.clear();
    m_functRootBlock = NULL;

    for(tInstrList::iterator it = m_instrs.begin(); it != m_instrs.end(); it++)
    {
        delete *it;
        *it = NULL;
    }

    m_instrs.clear();
}


void Function::addArg(const eDataType type, const string name)
{
    Symbol* tmp = m_functRootBlock->addSymbol(type, name, oArgument);
    m_args.push_back(tmp);
}


void Function::nameArg(const eDataType type, const string name)
{
    int i = 1;
    Symbol* tmpSymbol = NULL;

    for( vector<Symbol*>::iterator it = m_args.begin(); it != m_args.end(); it++ )
    {

        if( (*it)->getName().find("#") != string::npos )
        {
            // this argument is temporary change it name
            tmpSymbol = *it;

            //if( tmpSymbol->getName() != string("#" + ))
            // skontroluj poradie ci sedi

            if( type == tmpSymbol->getType() )
                tmpSymbol->setName(name);
            else
                WARNING("Types of arguments do not match.")

            break;
        }

        i++;
    }

    if( tmpSymbol == NULL )
    {
        // ading arg where have been declared less
        WARNING("Too many arguments.")
    }
}


bool Function::testArgRenamed() const
{
    for( vector<Symbol*>::const_iterator it = m_args.begin(); it != m_args.end(); it++ )
    {
    	if( (*it)->getName().find("#") != string::npos )
            return false;
    }

    return true;
}


void Function::instr_add(const eOpType instr, const eDataType type, const Operand* res, const Operand* op1, const Operand* op2)
{
    // check function semantic

    // generate new instruction
    Instruction* tmpInstr = new Instruction(instr, type, res, op1, op2);

    INFO("Function \"" << m_name << "\" added an instruction: " << instr)

    // add it
    m_instrs.push_back(tmpInstr);
}


void Function::enterIfInit()
{
    m_controls.push(IfStructure());
}


void Function::enterIfTrue()
{

}


void Function::enterIfFalse()
{

}


string Function::asString(const unsigned int indent) const
{
    stringstream ss;

    // function header
    ss << getIndent(indent) << m_retType << " " << m_name << " (";

    for(vector<Symbol*>::const_iterator it = m_args.begin(); it != m_args.end(); it++)
        ss << ((*it) ? (*it)->asString(true) : "NULL") << ", ";

    if(m_args.size() >= 1)
        ss << "\b\b)" << endl;      // erase last ','
    else
        ss << ")" << endl;


    // function body
    int i = 0;

    for(tInstrList::const_iterator it = m_instrs.begin(); it != m_instrs.end(); it++)
    {
        ss << getIndent(indent+1) << i++ << "# " << (*it)->asString() << endl;
    }

    return ss.str();
}
