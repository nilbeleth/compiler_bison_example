#include <sstream>

#include "file.h"
#include "utils.h"
#include "logger.h"


using namespace std;
using namespace symbols;



File::File(const string name)
    : m_filename(name)
{
    m_rootBlock = new Block(bGlobal);
}

File::~File()
{
    delete m_rootBlock;

    for( map<string,Function*>::iterator it = m_functTable.begin(); it != m_functTable.end(); it++)
    {
    	delete it->second;
    }

    m_functTable.clear();
}


Function* File::addFunction(const std::string name, const eDataType type)
{
    Function* tmpFunc = findFunction(name);
    if( tmpFunc == NULL )
    {
        tmpFunc = new Function(name, type);
        m_functTable[name] = tmpFunc;
    }
    else
    {
        // function find... is it just declared?
        if( tmpFunc->getDeclared() == uDeclared )
        {
            // check/expand arguments
        }
        else
        {
            WARNING("Function \"" << name << "\" already defined.")
        }
    }


    return tmpFunc;
}


Function* File::findFunction(const string name)
{
    if( m_functTable.find(name) == m_functTable.end() )
        return NULL;

    return m_functTable[name];
}


string File::asString(const unsigned int indent) const
{
    stringstream ss;

    ss << getIndent(indent) << "Functions:" << endl;
    for( map<std::string, Function*>::const_iterator it = m_functTable.begin(); it != m_functTable.end(); it++)
    {
    	ss << it->second->asString(indent+1);
    }
    ss << getIndent(indent) << "Symbols:" << endl;
    ss << m_rootBlock->asString(indent+1);

    return ss.str();
}



/*************************************************************\
|*******************    FileTable    *************************|
\*************************************************************/
FileTable::FileTable()
{

}


FileTable::~FileTable()
{
    for( map<string,File*>::iterator it = m_table.begin(); it != m_table.end(); it++)
    {
    	delete it->second;
    }

    m_table.clear();
}


File* FileTable::addFile(const string name)
{
    File* tmpFile = new File(name);
    m_table[name] = tmpFile;

    return tmpFile;
}


File* FileTable::findFile(const std::string file)
{
    (void) file;
    WARNING("Not yet implemented.")
    return NULL;
}


string FileTable::asString() const
{
    int i = 0;
    stringstream ss;

    for( map<string, File*>::const_iterator it = m_table.begin(); it != m_table.end(); it++)
    {
    	ss << i++ << "# " << it->first << ":" << endl;
    	ss << it->second->asString(1);
    }
    return ss.str();
}
