/*
 *
 *
 *
 */

 /*** Flex Declarations and Options ***/

 /* enable c++ scanner class generation */
%option c++

 /* count line numbers automatically */
%option yylineno

 /* do not use yywrap function/method */
%option noyywrap

 /* generate file with *.cpp */
%option outfile="lex.yy.cpp"

 /* we do not need unistd.h */
%option nounistd

%{
#include "scanner.h"
%}

SPACE			[ \t\n\r\f\v]
LOWER			[a-z]
UPPER			[A-Z]
ALPHA			({LOWER}|{UPPER})
DIGIT			[0-9]
ALNUM			({ALPHA}|{DIGIT})
ESCAPES			[nt\\'\"]
PRINT			[^\0-\31]

%%

{SPACE}+				/* skip white spaces */
({ALPHA}|_)({ALNUM}|_)*			return frontend::T_ID;
-?{DIGIT}+				return frontend::T_NUM;
\'(({PRINT})|(\\.))\'			return frontend::T_CHAR;
\"([^\\\"\n]|\\.)*\"			return frontend::T_STRING;
"("					return frontend::T_OP_LPAR;
")"					return frontend::T_OP_RPAR;
"{"					return frontend::T_OP_LBRA;
"}"					return frontend::T_OP_RBRA;
"="					return frontend::T_OP_ASSIGN;
"+"					return frontend::T_OP_PLUS;
"-"					return frontend::T_OP_MINUS;
"*"					return frontend::T_OP_TIMES;
"/"					return frontend::T_OP_DIVIDE;
"%"					return frontend::T_OP_MODULO;
">"					return frontend::T_OP_GREATER;
">="					return frontend::T_OP_GREATER_EQ;
"<"					return frontend::T_OP_LESS;
"<="					return frontend::T_OP_LESS_EQ;
"=="					return frontend::T_OP_EQUAL;
"!="					return frontend::T_OP_NONEQUAL;
"!"					return frontend::T_OP_NEG;
"||"					return frontend::T_OP_OR;
"&&"					return frontend::T_OP_AND;
","					return frontend::T_OP_COLON;
";"					return frontend::T_OP_SEMICOLON;
"/*"		{
			int c;
			while( (c = yyinput()) != 0 )
			{
				if( c == '*' )
				{
					if( (c = yyinput()) == '/' )
						break;
					else
						unput(c);
				}
			}
		}


"//"		{
			int c;
			while( (c = yyinput()) != 0 )
			{
				if( c == '\n' )
					break;
			}
		}

%%
