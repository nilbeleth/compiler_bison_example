#ifndef STACK_H
#define STACK_H
#include <vector>
#include <string>
#include <sstream>

#include "debug.h"



/**
 * @brief Deep stack implementation.
 *
 * In STL there is a stack which could only look at it's
 * first (top) item. This is not sufficient in our case so
 * we have to implement a Stack with read-only access to
 * deeper items (therefore deep stack).
 *
 * This class is implemented as wrapper around STL vector
 * providing additional funkcionality.
 */
template <typename T>
class Stack
{
    public:
        /** Default constructor */
        explicit Stack(const std::vector<T>& container = std::vector<T>());


        /** Default destructor */
        virtual ~Stack();


        /**
         * Push a new item on the top of the stack.
         * @param   item  The new item in stack.
         */
        void push(const T& item);


        /**
         * Deletes the topmost item of the stack.
         * @return  Return the deleted item.
         */
        void pop();


        /**
         * Gain access to n-th item on stack.
         * @param index     N-th item from top (in case of 0 it's top)
         */
        T& top(const int index = 0);


        const T& top(const int index = 0) const;


        /** The number of items in stack. */
        int size() const;


        /**
         * Is stack empty or not?
         * @return  Return true if stack is empty.
         */
        bool empty() const;


        /** Deallocate all items in stack */
        void clear();


        /** String representation of stack */
        std::string asString() const;


    private:
        // TODO (nilbeleth#1#): implement this or not?
        Stack(const Stack&);
        Stack& operator=(const Stack&);

        std::vector<T> m_container;             /**< Deep stack's data core */
};


using namespace std;



template <typename T>
Stack<T>::Stack(const std::vector<T>& container)
{
    m_container = container;
}


template <typename T>
Stack<T>::~Stack()
{
    m_container.clear();
}


template <typename T>
void Stack<T>::push(const T& item)
{
    m_container.insert(m_container.begin(), item);
}


template <typename T>
void Stack<T>::pop()
{
    m_container.erase(m_container.begin());
}


template <typename T>
T& Stack<T>::top(const int index)
{
    return m_container.at(index);
}


template <typename T>
const T& Stack<T>::top(const int index) const
{
    WARNING("Not implemented.")
    return NULL;
}


template <typename T>
int Stack<T>::size() const
{
    return m_container.size();
}


template <typename T>
bool Stack<T>::empty() const
{
    if( m_container.size() )
        return false;
    else
        return true;
}


template <typename T>
void Stack<T>::clear()
{
    m_container.clear();
}


template <typename T>
string Stack<T>::asString() const
{
    int i = 0;
    stringstream ss;

    for( typename vector<T>::const_iterator it = m_container.begin(); it != m_container.end(); it++)
    {
    	if( it == m_container.begin() )
    	{
    		ss << "top:\t";
    	}
    	else if( it == --m_container.end() )
    	{
    		ss << "bottom:\t";
    	}
    	else
            ss << i << ":\t";

        i++;
        ss << *it << endl;
    }

    return ss.str();
}
/*
template <class T>
  bool operator== ( const stack<T>& x, const stack<T>& y );

template <class T>
  bool operator< ( const stack<T>& x, const stack<T>& y );

template <class T>
  bool operator!= ( const stack<T>& x, const stack<T>& y );

template <class T>
  bool operator> ( const stack<T>& x, const stack<T>& y );

template <class T>
  bool operator>= ( const stack<T>& x, const stack<T>& y );

template <class T>
  bool operator<= ( const stack<T>& x, const stack<T>& y );
*/
#endif // STACK_H
