#include <cstdlib>
#include <iostream>

#include "file.h"
#include "settings.h"
#include "parser.h"
#include "logger.h"
#include "stack.h"


using namespace symbols;
using namespace frontend;
using namespace std;


int main(int argc, char* argv[])
{
    int errCode = eOk;


    PHASE("initialization")
    Settings& set = Settings::getInstance();
    errCode = set.parseArgs(argc, argv);
    if(errCode != 0)
        return EXIT_FAILURE;

    Logger& logger = Logger::getInstance();

    FileTable* fileTable = new FileTable;
    Parser* parser = new Parser(fileTable);



    PHASE("parsing")
    errCode = parser->parse();

    delete parser;
    parser = NULL;



//    if( logger.getErrorStatus() == eOk )
//    {
//        PHASE("generating target assambly language")
//
//        // generate target language
//    }



    PHASE("testing")
    std::cout << fileTable->asString() << std::endl;



    PHASE("cleaning up")
    delete fileTable;


    return logger.getErrorStatus();
}
