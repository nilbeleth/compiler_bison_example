#include "irgenerator.h"
#include "logger.h"


using namespace std;
using namespace symbols;
using namespace frontend;



/*    Constructors & destructors     */
IRGenerator::IRGenerator(FileTable* table)
    : m_type(tUnset), m_id(0), m_file(NULL), m_function(NULL)
{
    m_table = table;
}


IRGenerator::~IRGenerator()
{
    m_table = NULL;
    m_file = NULL;
    m_function = NULL;
    m_blocks.clear();
}


/*    File manipulation              */
void IRGenerator::addFile(const string name)
{
    m_file = m_table->addFile(name);
    m_blocks.push(m_file->getRootBlock());
}


/*     Function manipulation         */
void IRGenerator::selectFunct(const Token tok)
{
    if( m_function != NULL )
        DEBUG("Function already set.")

    if( (m_function = m_file->findFunction(tok.sValue)) == NULL )
    {
        m_function = m_file->addFunction(tok.sValue, m_type);
        m_blocks.top()->openNewBlock(m_function->getFunctRootBlock());
    }
    else
    {
        // check return type
        if( m_function->getRetType() != m_type )
            WARNING("Return type do not match with declaration.")
    }

    INFO("Set function \"" << m_function->getName() << "\" selected.")

    // check if it hasn't alredy been defined
    if( m_function->getDeclared() == uDefined )
        WARNING("Function already defined.")

    m_blocks.push(m_function->getFunctRootBlock());

    m_type = tUnset;
}


void IRGenerator::setFuncDeclared()
{
        if( m_function == NULL )
    {
        ERROR("No function and setting declared.")
        return;
    }

    if( m_function->getDeclared() != uUnknown )
        WARNING("Function already declared.")

    INFO("Set function \"" << m_function->getName() << "\" declared.")
    m_function->setDeclared();
    m_function = NULL;

    m_blocks.pop();         // pop out function block => general block should be there now
    m_type = tUnset;        // if no arguments there was left type from return type
}


void IRGenerator::enterFunct()
{
    if( m_function == NULL )
    {
        ERROR("No function and entering.")
        return;
    }

    // NOTE (nilbeleth#1#): este poriesit ak je v deklaracii void a definicii daco ine
    INFO("Entering function \"" << m_function->getName() << "\"")
    if( !m_function->testArgRenamed() )
        WARNING("Arguments do not match with declaration.")

    m_function->setDefined();

    m_type = tUnset;        // if no arguments there was left type from return type

    // initialize instruction list
}


void IRGenerator::leaveFunct()
{
        if( m_function == NULL )
    {
        ERROR("No function and leaving function.")
        return;
    }

    INFO("Leaving function \"" << m_function->getName() << "\"")

    // pop out the function block
    m_blocks.pop();

    // set function defined
    m_function->setDefined();

    m_function = NULL;
}


void IRGenerator::addArg(const Token tok)
{
    if( m_function == NULL )
    {
        ERROR("No function and adding arguments.")
        return;
    }

    //INFO("For \"" << m_function->getName() << "\" adding argument: " << m_type << " " << tok.sValue)
    if( m_function->getDeclared() == uDeclared )
        m_function->nameArg(m_type, tok.sValue);
    else
        m_function->addArg(m_type, tok.sValue);

    m_type = tUnset;
}


void IRGenerator::declArg()
{
    if( m_function == NULL )
    {
        ERROR("No function and declaring argument.")
        return;
    }

    //INFO("For \"" << m_function->getName() << "\" adding argument: " << m_type)
    stringstream arg_name;
    arg_name << "#" << m_function->getArgCount() +1;

    m_function->addArg(m_type, arg_name.str());     // substitute name for arguments yet

    m_type = tUnset;
}


/*      Statement manipulation          */
void IRGenerator::declareVar( const Token tok )
{
    // problem in parser so hardfix here
    if( tok.type != T_ID )
    {
        m_blocks.top()->addSymbol(m_type, m_token.sValue, oVariable);
        WARNING("Passed token isn't a ID.")
    }
    else
            m_blocks.top()->addSymbol(m_type, tok.sValue, oVariable);
}


void IRGenerator::declareVar( const Token tok1, const Token tok2 )
{
    Symbol* tmpSymbol = m_blocks.top()->addSymbol(m_type, tok1.sValue, oVariable);

    if( m_type == tChar )
        tmpSymbol->setValue(tok2.cValue);
    else if( m_type == tInt )
        tmpSymbol->setValue(tok2.iValue);
    else if( m_type == tString )
        tmpSymbol->setValue(tok2.sValue);
    else
        { ERROR("Bad data type.") }

    // unneccassry constant on stack => pop it out
    if( m_symStack.size() != 0 )
        m_symStack.pop();
}


void IRGenerator::defConst(const Token tok)
{
    //DEBUG("Defing variable: " << tok.iValue)
    eDataType type = tUnset;

    if( tok.type == T_NUM )
        type = tInt;
    else if( tok.type == T_CHAR )
        type = tChar;
    else if( tok.type == T_STRING )
        type = tString;
    else
        WARNING("Unknown data type for constant.")


    Symbol* tmpSymbol = NULL;   // TODO (nilbeleth#1#): generate unique name for variable
    string name = generateVariable(oConstant, tok.lineno);
    tmpSymbol = m_blocks.top()->addSymbol(type, name, oConstant);

    if( type == tInt )
        tmpSymbol->setValue(tok.iValue);
    else if( type == tChar )
        tmpSymbol->setValue(tok.cValue);
    else if( type == tString )
        tmpSymbol->setValue(tok.sValue);
    else
        WARNING("Unknown data type for constant.")

    m_symStack.push(tmpSymbol);
}


void IRGenerator::loadSymbol(const Token tok)
{
    // find such symbol
    Symbol* tmpSymbol = NULL;
    for( int i = 0; i < m_blocks.size(); i++)
    {
        tmpSymbol = m_blocks.top(i)->findSymbol(tok.sValue);

        if( tmpSymbol != NULL)
            break;
    }

    // check if there is any symbol
    if( tmpSymbol == NULL )
    {
        WARNING("No symbol \"" << tok.sValue << "\" found.")
        return;
    }

    // load this symbol to stack
    m_symStack.push(tmpSymbol);
}


void IRGenerator::assign(const Token tok)
{
    // prepare participants
    if( m_blocks.size() < 1 )
    {
        WARNING("No symbol on stack.")
        return;
    }
    Symbol* op = m_symStack.top();
    m_symStack.pop();

    Symbol* target = NULL;
    for( int i = 0; i < m_blocks.size(); i++)
    {
        target = m_blocks.top(i)->findSymbol(tok.sValue);

        if( target != NULL)
            break;
    }

    // check if there is any symbol
    if( target == NULL )
    {
        WARNING("No symbol \"" << tok.sValue << "\" found.")
        return;
    }


    // check types
    if( op->getType() != target->getType() )
    {
    	WARNING("Incompatible types in assigment")
    	return;
    }

    // add instruction
    m_function->instr_add(iAssign, op->getType(), target, op);
}


void IRGenerator::ret(const bool expr)
{
    Symbol* retValue = NULL;


    if( m_function->getRetType() == tVoid && expr )
    {
        WARNING("Returning expression in void function.")
    }
    if( expr )
    {
        // is there any symbol on stack? (there really should be)
        if( m_symStack.size() < 1 )
        {
            WARNING("There is no expression on stack.")
            return;
        }

        // get the symbol from stack
        retValue = m_symStack.top();
        m_symStack.pop();

        if( m_function->getRetType() != retValue->getType() )
        {
            WARNING("The return type do not match function return type.")
            return;
        }
    }

    // generate instruction
    m_function->instr_add(iReturn, m_function->getRetType(), retValue);
}


void IRGenerator::jump(const bool daco)
{
    WARNING("Not implememented.")
}


void IRGenerator::enterIfCond()
{
    //m_controls.push(new IfStructure());
    if( m_function == NULL )
    {
        ERROR("No function loaded.")
        return;
    }
}


void IRGenerator::enterIfTrue()
{

}


void IRGenerator::enterIfFalse()
{

}


void IRGenerator::evalUnaOp(const Token tok)
{
    if( m_symStack.size() < 1 )
    {
        ERROR("Unsufficient operands on stack: " << m_symStack.size())
        return;
    }

    DEBUG("Operator: " << tok.type)
}


void IRGenerator::evalBinOp(const Token tok)
{
    // check if we have enough operands (this really shouldn't happen)
    if( m_symStack.size() < 2 )
    {
        ERROR("Unsufficient operands on stack: " << m_symStack.size())
        return;
    }


    // get operands from stack
    Symbol* op2 = m_symStack.top();
    m_symStack.pop();
    Symbol* op1 = m_symStack.top();
    m_symStack.pop();


    // process operator and check types
    eOpType instrType = iNop;
    eDataType resType = tUnset;
    switch(tok.type)
    {
    	case T_OP_PLUS:
            if( op1->getType() ==tInt && op2->getType() == tInt )
                resType = tInt;
            else
                WARNING("Forbidden operation: " << op1->getType() << " + " << op2->getType() )

            instrType = iAdd;
            break;
    	case T_OP_MINUS:
            if( op1->getType() ==tInt && op2->getType() == tInt )
                resType = tInt;
            else
                WARNING("Forbidden operation: " << op1->getType() << " - " << op2->getType() )

            instrType = iSub;
            break;
    	case T_OP_TIMES:
            if( op1->getType() ==tInt && op2->getType() == tInt )
                resType = tInt;
            else
                WARNING("Forbidden operation: " << op1->getType() << " * " << op2->getType() )

            instrType = iMult;
            break;
    	case T_OP_DIVIDE:
            if( op1->getType() ==tInt && op2->getType() == tInt )
                resType = tInt;
            else
                WARNING("Forbidden operation: " << op1->getType() << " / " << op2->getType() )

            instrType = iDiv;
            break;
    	case T_OP_MODULO:
            if( op1->getType() ==tInt && op2->getType() == tInt )
                resType = tInt;
            else
                WARNING("Forbidden operation: " << op1->getType() << " % " << op2->getType() )

            instrType = iMod;
            break;
    	default:
            ERROR("Undefined operation: " << tok.type )
            return;
    }


    // create new symbol for result
    string name = generateVariable(oExpression, tok.lineno);
    Symbol* res = m_blocks.top()->addSymbol(resType, name, oExpression);


    // generate instruction
    DEBUG("Instruction: " << res->getName() << " = " << op1->getName() << " " << tok.type << " " << op2->getName())
    m_function->instr_add(instrType, resType, res, op1, op2);


    // push back result
    m_symStack.push(res);
}


/*       Auxilary functions           */
void IRGenerator::saveID(const Token tok)
{
    m_token = tok;
}


void IRGenerator::unsetType()
{
    if( m_type == tUnset )
        DEBUG("Type not set (already).")

    m_type = tUnset;
}


void IRGenerator::setType( const eDataType type )
{
    if( m_type != tUnset )
        DEBUG("Type is set: " << m_type)

    m_type = type;
}


string IRGenerator::generateVariable(const eOrigin orig, const unsigned int lineno)
{
    stringstream ss;

    ss << "$";

    switch(orig)
    {
    	case oConstant:
            ss << "const";
    		break;
        case oExpression:
            ss << "expr";
            break;
    	default:
            WARNING("Unknown origin type.")
    }

    ss << "@" << lineno << "#" << m_id++;

    return ss.str();
}
