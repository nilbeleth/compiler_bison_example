#include <iostream>

#include "logger.h"
#include "debug.h"
#include "settings.h"


using namespace std;



Logger::Logger()
    : m_errStatus(eOk)
{
    // probably set verbose level
}


Logger& Logger::getInstance()
{
    static Logger instance;
    return instance;
}


void Logger::log(string msg, TDebugClass severity)
{
    WARNING("Not yet implemented.")
    (void) msg;
    (void) severity;
    // for this purpose we use macros from debug.h
}


void Logger::logWarning(const string msg, const int lineno)
{
    ERROR("Not yet implemented.")
}


void Logger::logError(const TErrorClass errType, const string msg, const int lineno)
{
    if( lineno == -1 )
    {
        if( errType == eLexical || errType == eSyntactical || errType == eSemantical )
        {
            WARNING("Line number requested but not presented.")
            return;
        }
    }


    m_errStatus = ( m_errStatus == eOk ) ? errType : m_errStatus;

    Settings& settings = Settings::getInstance();

    if( errType == eLexical || errType == eSyntactical || errType == eSemantical )
        cout << settings.getInputFile() << ":" << lineno << ": " << msg << endl;
    else if( errType == eGeneration || errType == eInternal )
        cout << settings.getInputFile() << ": " << msg << endl;
    else
        WARNING("Unknown error.")

    return;
}


int Logger::getErrorStatus() const
{
    return m_errStatus;
}
