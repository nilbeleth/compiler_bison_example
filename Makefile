# Project:  C to MIPS32 Compilator
# Author:   Matej Odalos <xodalo00@stud.fit.vutbr.cz>
# Date:     30.09.2012
#
# Usage:
#


SRC = ./src
BUILD = ./obj
SOURCES = main.cpp settings.cpp scanner.cpp lex.yy.cpp logger.cpp parser.cpp \
	  irgenerator.cpp bison_parser.cpp file.cpp function.cpp symbol.cpp \
	  utils.cpp instruction.cpp controlstructure.cpp

OBJECTS = $(patsubst %,$(BUILD)/%,$(SOURCES:.cpp=.o))
CXX = g++
CPPFLAGS = 						# preprocessor flags
CXXFLAGS = -std=c++98 -Wall -Wextra -pedantic -W -g	# compile flags
LDFLAGS =						# linker flags

LIBS = 

PROG = compiler



#
# Object targets
#

all: pre-build build

pre-build:
	mkdir -p $(BUILD)
	cd $(SRC) && flex scanner.ll
	cd $(SRC) && bison parser.yy


build: $(PROG)


$(PROG): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@ $(LIBS)


$(BUILD)/%.o: $(SRC)/%.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<




pack:
	tar cvvf xodalo00.tar src/* Makefile doc/*

clean:
	rm -rf $(BUILD)
	rm -rf $(SRC)/lex.yy.cpp
	rm -rf $(SRC)/bison_parser.cpp $(SRC)/bison_parser.hpp $(SRC)/location.hh $(SRC)/position.hh $(SRC)/stack.hh
	rm -f doxygen.log
	rm -rf ./doc/html
	make clean -C ./doc/tex/

purge:
	rm -rf $(BUILD)
	rm -rf $(SRC)/lex.yy.cpp
	rm -rf $(SRC)/bison_parser.cpp $(SRC)/bison_parser.hpp location.hh position.hh stack.hh
	rm -rf $(PROG)
	rm -rf xodalo00.tar
	rm -rf doxygen.log
	rm -rf ./doc/html

doc:
	make -C ./doc/tex/

doxygen:
	doxygen ./doc/doxygen.cfg
